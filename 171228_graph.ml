module type QUEUE = sig 
  exception EmptyQueue
  type 'a queue 
  val empty : 'a queue
  val is_empty : 'a queue ->bool 
  val insert : 'a queue ->'a ->'a queue
  val front : 'a queue ->'a 
  val remove : 'a queue ->'a queue 
end;;

module Fifo : QUEUE = struct exception EmptyQueue
  (* Kolejka to trójka: przód,tył,rozmiar. *)
  (* Jeżeli przód jest pusty,to kolejka jest pusta. *) 
  type 'a queue = {front: 'a list; back: 'a list; size: int}
  let empty = {front=[]; back=[]; size=0}
  let size q = q.size
  let is_empty q = size q = 0
  let balance q = match q with {front=[]; back=[]} ->q | {front=[]; back=b; size=s} ->{front=List.rev b; back=[]; size=s} | _ ->q
  let insert {front=f; back=b; size=n} x = balance {front=f; back=x::b; size=n+1}
  let front q = match q with {front=[]} ->raise EmptyQueue | {front=x::_} ->x
  let remove q = match q with {front=_::f} ->balance {front=f; back=q.back; size=q.size-1} | _ ->raise EmptyQueue 
end;;

module type GRAPH_LABELS = sig 
  type label 
end;;
module type GRAPH = sig 
  (* Etykiety krawędzi. *)
  type label
  (* Typ grafów o wierzchołkach typu 'a. *)
  type graph
  (* Pusty graf. *)
  val init : int ->graph
  (* Rozmiar grafu *)
  val size : graph ->int
  (* Dodanie krawędzi skierowanej łączącej dwa wierzchołki. *) 
  val insert_directed_edge : graph ->int ->int ->label ->unit
  (* Dodanie krawędzi nieskierowanej (dwukierunkowej) łączącej dwa wierzchołki. *) 
  val insert_edge : graph ->int ->int ->label ->unit
  (* Lista incydencji danego wierzchołka. *) 
  val neighbours : graph ->int ->(int*label) list end;;

module Graph = functor (L : GRAPH_LABELS) ->
  (struct
    (* Etykiety krawędzi. *)
    type label = L.label
    (* Typ grafów - tablica list sąsiedztwa. *)
    type graph = {n : int; e : (int*label) list array}
    (* Pusty graf. *) 
    let init s = {n = s; e = Array.make s []}
    (* Rozmiar grafu. *)
    let size g = g.n
    (* Dodanie krawędzi skierowanej łączącej dwa wierzchołki. *) 
    let insert_directed_edge g x y l = assert ((x<>y) && (x >= 0) && (x <size g) && (y >= 0) && (y <size g) && (List.filter (fun (v,_) ->v=y) g.e.(x) = [])); g.e.(x) <- (y,l)::g.e.(x)
    (* Dodanie krawędzi łączącej dwa (istniejące) wierzchołki. *)
    let insert_edge g x y l = insert_directed_edge g x y l; insert_directed_edge g y x l
    (* Lista incydencji danego wierzchołka. *)
    let neighbours g x = g.e.(x) end : GRAPH with type label = L.label);;

module GraphSearch (Q : QUEUE) (G : GRAPH) = struct
  open G
  let search visit g = 
    let visited = Array.make (size g) false in 
    let walk x color = let q = ref Q.empty in
      begin 
        q := Q.insert !q x;
        visited.(x) <- true; 
        while not (Q.is_empty !q) do
          let v = Q.front !q in 
          begin visit v color;
            q := Q.remove !q;
            List.iter (fun (y,_) -> if not (visited.(y)) then 
            begin 
              q := Q.insert !q y; 
              visited.(y)<- true 
            end) (neighbours g v); 
          end 
        done
      end in (* Kolor *) 
    let k = ref 0 in 
    for i = 0 to size g do if not visited.(i) then 
        begin (* Kolejna składowa. *) walk i (!k);
          k := !k+1 
        end; 
    done;
    !k
end;;

module BFS = GraphSearch (Fifo);; 

module Lifo : QUEUE = struct
  exception EmptyQueue
  type 'a queue = 'a list
  let empty = [] 
  let is_empty q = q = []
  let insert q x = x::q 
  let front q = if q = [] 
    then raise EmptyQueue else List.hd q 
  let remove q = if q = [] then raise EmptyQueue else List.tl q end;;
module DFS = GraphSearch (Lifo);;

