type 'a bin_tree = Node of 'a bin_tree * 'a * 'a bin_tree | Null;;

let rec fold_tree f a t = match t with
    | Null -> a
    | Node(l, v, r) -> 
        let wynik_l = fold_tree f a l
        and wynik_r = fold_tree f a r
        in f wynik_l v wynik_r;;

let rec map_tree f t = fold_tree (fun l v r -> Node(map_tree f l, f v, map_tree f r)) Null t

let wezly t = fold_tree (fun l _ r -> l + r + 1) 0 t;;

let wysokosc t = fold_tree (fun l _ r -> 1 + max l r) 0 t;;

let srednica t = snd (fold_tree (fun (lh, ld) _ (rh, rd) -> (1 + max lh rh, max (max ld rd) (lh + rh))) (0, 0) t);;

let czy_bst t = let (_, _, bst) = fold_tree (fun (lmax, lmin, lbst) v (rmax, rmin, rbst) ->
 (max (max lmax v) rmax, min (min rmin v) lmin, lbst && (lmax < v) && (rmin > v))
) (neg_infinity, infinity, true) t in bst