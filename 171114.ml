open List;;

let binary n = 
    let rec aux reszta lista =
    if reszta = 0 then lista else
    aux (reszta / 2) ((reszta mod 2)::lista)
    in aux n [];;

let decimal list = 
    fold_left (fun acc h -> h + acc * 2) 0 list;;

let czy_rzadka n = 
    let rec aux reszta poprzednia rzadka =
        if reszta = 0 then rzadka else
        let (reszt, rzad) = (reszta / 2, reszta mod 2)
        in aux reszt rzad (rzadka && not (rzad = poprzednia && rzad = 1))
    in aux (n / 2) (n mod 2) true;;

let nast_rzad_slow n = 
    let rec aux rzad =
        if rzad = n || not (czy_rzadka rzad) then aux (rzad + 1) else rzad
    in aux n;;

(* let nast_rzad n =
    let aux h (przod, zera, carry) = 
        let poprzednia = match przod with
        | [] -> 0
        | head::_ -> head
        in if poprzednia = 1 && h = 1 then
            ([], zera + (length przod), carry)
        else


    let bin = rev (binary n) in 
    fold_left (fun h (przod, zera, poprzednia) -> 
        if h = 1 && poprzednia = 1 then ([], zera + (length przod), 1)
        else (h::przod, zera, h)
    ) *)

let dekompresuj lista =
    let buduj rozpisane = 
        let rec aux reszta acc =
            match reszta with
            | [] -> acc
            | (co, 0)::t ->  aux t acc
            | (co, ile)::t -> aux ((co, ile - 1)::t) (co::acc)
        in rev (aux rozpisane [])
    and rozpisz n = 
        let rec aux (liczba, razy) =
            if liczba mod 2 = 1 then
                ((liczba + 1) / 2, razy)
            else aux (liczba / 2, razy + 1)
        in aux (n, 1)
    in let rozpisane = map rozpisz lista
    in buduj rozpisane;;

type 'a tree = Node of  'a * 'a tree * 'a tree | Null;;

let rec fold_tree f acc t = 
    match t with
    | Null -> acc
    | Node(v, l, r) -> f  v (fold_tree f acc l) (fold_tree f acc r);;

let skosokosc t = 
    fold_tree (fun v (ls, lt) (rs, rt) -> 
         if ls < rs then (max (ls * 2 + 1) (rs * 2), Node(v, rt, lt))
            else if ls = rs then (rs * 2 + 1, Node(v, lt, rt))
            else max (rs * 2 + 1) (ls * 2), Node(v, lt, rt)) (0, Null) t;;

let czy_symetryczne t =
    let rec aux t1 t2 =
        match (t1, t2) with
        | (Null, Null) -> true
        | (Node(v1, l1, r1), Node(v2, l2, r2)) -> v1 = v2 && (aux l1 r2) && (aux l2 r1)
        | _ -> false
    in match t with
    | Null -> true
    | Node(v, l, r) -> aux l r;;

let liscie t =
    let rec aux tr = 
        match tr with
        | Null -> []
        | Node(v, Null, Null) -> [v]
        | Node(_, l, r) -> (aux l) @ (aux r) in 
    aux t;;

let liscie_fast t =
    let rec aux tr lista =
        match tr with
        | Null -> lista
        | Node(v, Null, Null) -> v::lista
        | Node(_, l, r) -> aux l (aux r lista) in 
    aux t [];;

let rec fold_tree f acc t = 
    match t with
    | Null -> acc
    | Node(v, l, r) -> f  v (fold_tree f acc l) (fold_tree f acc r);;

(* let czapeczka t = 
    let rec aux tr1 tr2 ile = 
        match (tr1, tr2) with
        | (Null, Null) -> (true, ile)
        | (Node(v1, l1, r1), Node(v2, l2, r2)) -> v1 = v2 && fst (aux l1 r1 ile) && fst (aux l1 r2 ile) && fst (aux l2 r2 ile) 
        | _ -> (false, ile) in 
    match t with
    | Null -> 0
    | Node(v, l, r) -> snd (aux l r 1) *)

