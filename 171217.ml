open List;;
let widoczne l =
    let monotonizuj kolejka wysokosc = 
        let rec aux rest wynik = 
            match rest with
            | [] -> [(wysokosc, wynik)]
            | (stara, stara_wynik)::t -> 
                if stara > wysokosc then 
                    (wysokosc, wynik)::rest
                else 
                    aux t (wynik  + 1)
        in aux kolejka 0
    in 
    let rec aux rest wyniki kolejka = 
        match rest with
        | [] -> rev wyniki
        | h::t -> let nowa_kolejka = monotonizuj kolejka h in 
        aux t ((nowa_kolejka |> hd |> snd)::wyniki) nowa_kolejka in 
    let lewe = aux l [] []
    and prawe = rev (aux (rev l ) [] []) in 
    map2 (fun l p -> l + p) lewe prawe;;

type 'a tree = Null | Node of 'a * 'a tree * 'a tree * 'a tree ref ;;
let fastryguj drzewo =
    let ost = ref Null in 
    let rec pom t = 
        match t with 
        | Null -> ()
        | Node(x, l, p, rx) -> 
        begin 
            pom l;
            (match !ost with
            | Null -> rx := t
            | Node(y, _, _, ry) -> 
                if x = y then begin
                    rx := !ry;
                    ry := t;
                end else begin
                    rx := t;
                end);
            ost := t;
            pom p;
        end
    in pom drzewo