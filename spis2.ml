fold_left : ('a -> 'b -> 'a) -> 'a -> 'b list -> 'a
fold_right : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b
map : ('a -> 'b) -> 'a list -> 'b list
map2 : ('a -> 'b -> 'c) -> 'a list -> 'b list -> 'c list
fold_left2 : ('a -> 'b -> 'c -> 'a) -> 'a -> 'b list -> 'c list -> 'a
filter : ('a -> bool) -> 'a list -> 'a list
partition : ('a -> bool) -> 'a list -> 'a list * 'a list

type 'a bin_tree = Node of 'a bin_tree * 'a * 'a bin_tree | Null;;

let rec fold_tree f a t = match t with
    | Null -> a
    | Node(l, v, r) -> 
        let wynik_l = fold_tree f a l
        and wynik_r = fold_tree f a r
        in f wynik_l v wynik_r;;
let czy_bst t = let (_, _, bst) = fold_tree (fun (lmax, lmin, lbst) v (rmax, rmin, rbst) ->
 (max (max lmax v) rmax, min (min rmin v) lmin, lbst && (lmax < v) && (rmin > v))
) (neg_infinity, infinity, true) t in bst

open List;;

type 'a tree = Node of 'a tree * 'a * 'a tree | Empty;;

let rec fold_tree f acc t = 
    match t with
    | Empty -> acc
    | Node(l, v, r) -> f (fold_tree f acc l) v (fold_tree f acc r);;

let map_tree f t = 
    fold_tree (fun l v r -> Node(l, f v, r)) Empty t;;

type 'a nbtree = NbNode of 'a * 'a nbtree list;;

let rec fold_nbtree f (NbNode (x, l)) = 
    f x (map (fold_nbtree f) l);;

let map_nbtree f t = fold_nbtree (fun x l ->NbNode (f x,l)) t;;

let nbwidoczne t = 
    let rec merge (element: int) (l: (int -> int) list) (maks: int) = 
        if element < maks then
            fold_left (fun acc h -> acc + h maks) 0 l
        else
            fold_left (fun acc h -> acc + h element) 0 l + 1
    in
        fold_nbtree merge t (-1);;

(* wartość w węźle drzewa jest widocza jeżeli na ścieżce od tego węzła 
    do korzenia nie ma większej wartości *)
let widoczne (t: int tree) = 
    (* zwraca funkcję, która jako argument przyjmuje maksimum na ścieżce
        od korzenia do korzenia danego poddrzewa
        i oblicza liczbę elementów widocznych w poddrzewie *)
    let rec merge (lewy: int -> int) (element: int) (prawy: int -> int) =
        fun (maks: int) ->
        if element < maks then 
            lewy maks + prawy maks
        else 
            lewy element + prawy element + 1
    in match t with
    | Empty -> 0
    | Node(_, v, _) -> (fold_tree merge (fun _ -> 0) t) (v);;

(* wzrost [1; 2; 1; 2; 3];; *)
(* # wzrost [3;4;0;(-1);2;3;7;6;7;8];; *)
let wzrost (l: int list) =
    let aux (najdluzszy, naj_len, obecny, obecny_len) (head: int) =
        match obecny with
        | [] -> (najdluzszy, naj_len, [head], 1)
        | h::_ -> if head > h then (najdluzszy, naj_len, head::obecny, obecny_len + 1) 
            else if (obecny_len) >= (naj_len) then (obecny, obecny_len, [head], 1)
            else (najdluzszy, naj_len, [head], 1)
    in let (najdluzszy, naj_len, obecny, obecny_len) = (fold_left aux ([], 0, [], 0) l)
    in rev (if obecny_len >= naj_len then obecny else najdluzszy) ;;


let levels tree =
   (fold_tree (fun l x r -> 
    fun lista -> 
    match lista with
    | [] -> [x]::(l (r ([])))
    | h::t -> (x::h)::(l (r (t))))
   (fun lista -> lista) tree) [[]];;

let infix t = (fold_tree (fun l x r -> fun lista -> l (x::(r lista))) (fun x -> x) t ) [];;

let skosokosc t = 
    fold_tree (fun v (ls, lt) (rs, rt) -> 
         if ls < rs then (max (ls * 2 + 1) (rs * 2), Node(v, rt, lt))
            else if ls = rs then (rs * 2 + 1, Node(v, lt, rt))
            else max (rs * 2 + 1) (ls * 2), Node(v, lt, rt)) (0, Null) t;;

let czy_symetryczne t =
    let rec aux t1 t2 =
        match (t1, t2) with
        | (Null, Null) -> true
        | (Node(v1, l1, r1), Node(v2, l2, r2)) -> v1 = v2 && (aux l1 r2) && (aux l2 r1)
        | _ -> false
    in match t with
    | Null -> true
    | Node(v, l, r) -> aux l r;;

let liscie_fast t =
    let rec aux tr lista =
        match tr with
        | Null -> lista
        | Node(v, Null, Null) -> v::lista
        | Node(_, l, r) -> aux l (aux r lista) in 
    aux t [];;

val make_matrix : int -> int -> 'a -> 'a array array
val iteri : (int -> 'a -> unit) -> 'a array -> unit
val iter : ('a -> unit) -> 'a array -> unit
val map2 : ('a -> 'b -> 'c) -> 'a array -> 'b array -> 'c array
val sort : ('a -> 'a -> int) -> 'a array -> unit
val init : int -> (int -> 'a) -> 'a array

a := 0;;
for i = 1 to 2 do
  a := !a + 1;
done;;
a = 2

let lewe_liscie drzewo =
    let rec aux t = 
    match t with 
    | Null -> None
    | Node(v, l, r, p) -> 
            match (aux l, aux r) with
            | None, None -> p := None; Some(v)
            | None, Some(x) -> p:= Some(x); Some(x)
            | Some(y), _ -> p:=Some(y); Some(y)
    in aux drzewo; ()

let w_lewo drzewo = 
    let q = Queue.create() in
    Queue.add (drzewo, 1) q;
    while not (Queue.is_empty q) do 
        match Queue.take q with
        | (Null, _) -> ()
        | (Node(v, l, r, p), h) -> 
        if not (Queue.is_empty q) then
            match Queue.peek q with
            | (Node(vv, _, _, _), hh) when hh = h -> p := Some(vv)
            | _ -> p:=None
        else
            p := None;
        if r <> Null then Queue.add (r, h + 1) q;
        if l <> Null then Queue.add (l, h + 1) q;
    done

let potomek drzewo = 
    let rec aux t h = 
    match t with
    | Null -> (None, -1)
    | Node(v, l, r, p) -> 
        let (lewy, lh) = aux l (h+1)
        and (prawy, rh) = aux r (h+1)
        in 
        if (lewy = None && prawy = None) then begin
            p:= None;
            (Some(v), h)
        end else if lh >= rh then begin
            p:= lewy;
            (lewy, lh)
        end else begin              
            p:= prawy;
            (prawy, rh)
        end;
    in aux drzewo 0; ()


let ustaw l = 
    let li = ref l in
    let odw = ref (rev l) in 
    while (!odw <> []) do 
        (hd !li).prev <- (!odw);
        odw := tl !odw;
        li := tl !li;
    done

let fastryguj drzewo =
    let ost = ref Null in 
    let rec pom t = 
        match t with 
        | Null -> ()
        | Node(x, l, p, rx) -> 
        begin 
            pom l;
            (match !ost with
            | Null -> rx := t
            | Node(y, _, _, ry) -> 
                if x = y then begin
                    rx := !ry;
                    ry := t;
                end else begin
                    rx := t;
                end);
            ost := t;
            pom p;
        end
    in pom drzewo


let katastrofy_po_ludzku l = 
    let monotonizuj kolejka katastrofa = 
        let rec aux rest wiek = 
            match rest with
            | [] -> [(katastrofa, wiek)]
            | (stara, stara_wiek)::t -> 
                if stara > katastrofa then 
                    (katastrofa, wiek)::rest
                else 
                    aux t (wiek + stara_wiek)
        in aux kolejka 1
    in 
    let rec aux rest wyniki kolejka = 
        match rest with
        | [] -> rev wyniki 
        | katastrofa::t -> 
            let nowa_kolejka = monotonizuj kolejka katastrofa in 
            aux t ((nowa_kolejka |> hd |> snd)::wyniki) nowa_kolejka in 
    aux l [] [];;

let cykl arr = 
    let n = length arr in
    let odwiedzone = make n false in 
    let rec przejdz start i dlugosc = 
        odwiedzone.(i) <- true;
        if i = start then dlugosc
        else przejdz start (arr.(i)) (dlugosc + 1) 
    in let rec loop i maks = 
        if i = n  then maks
        else if not (odwiedzone.(i)) then loop (i + 1) (max (przejdz i (arr.(i)) 1) maks) 
        else loop (i + 1) maks
    in loop 0 1;;

let rotacja a k = 
    let move l r kl = 
        for i = 0 to (kl-1) do 
            let temp = a.(l+i) in 
            begin 
                a.(l+i) <- a.(r+i-kl);
                a.(r+i-1) <- temp;
            end
        done in 
    let rec aux l r kl = 
        let n = r - l in 
        if n <> kl then
            if kl < n/2 then begin
                move l r kl;
                aux l (r-kl) kl
            end else begin 
                move l r (n-kl);
                aux (r-kl) r (2*kl-n)
            end
    in aux 0 (length a) k;;

(* [x1; x2; ...; xn] posortowana. k razy! bierzemy dwa najmniejsze, dodajemy i wkładamy do listy
    trzymamy sobie listę i heapa. bierzemy najmniejszy z listy i najmniejszy z heapa (lub odpowiednio dwa) i dodajemy. 
    złożoność będzie k*logk *)


let kubeczki l k = 
    let wez_min lista kolejka = 
    if is_empty kolejka then match lista with
        | h::t -> (h, t, kolejka)
        | [] -> failwith "nie ma z czego"
    else let min_kolejki = get_min kolejka in 
        match lista with
        | h::t -> if h < min_kolejki then (h, t, kolejka) else (min_kolejki, lista, del_min kolejka)
        | [] -> (min_kolejki, [], del_min kolejka)
    in let rec aux lista kolejka rest =
        if rest = 0 then let (min, _, _) = wez_min lista kolejka in min 
        else let (pierwsze, n_l, n_k) = wez_min lista kolejka in 
            let (drugie, f_l, f_k) = wez_min n_l n_k in 
            aux f_l (push f_k (pierwsze + drugie)) (rest - 1) in 
        aux l Empty k;;


let polacz laczenia klasy = 
  List.iter (fun (a, b) -> union klasy.(a) klasy.(b)) laczenia

(* (a,b) oznacza, ze a trzyma b lub a puszcza b. Zawsze taka kolejnosc *)
let malpki n trzymania puszczenia = 
  let klasy = Array.init n (fun x -> make_set x) in 
  let nierozlaczne = diff trzymania puszczenia in 
  polacz nierozlaczne klasy;
  let czas = length puszczenia in 
  let momenty = Array.init n (fun x -> -1) in 
  puszczenia |> rev |> iteri 
    begin fun i (a, b) -> 
      elements (klasy.(a)) |> iter begin fun x ->
        momenty.(x) <- (czas - i)
      end;
      union klasy.(a) klasy.(b);
    end;
  let pierwsza = klasy.(0) in
  klasy |> Array.iteri (fun i kl ->
    if not (equivalent kl pierwsza) then begin 
      momenty.(i) <- 0
    end);
  momenty