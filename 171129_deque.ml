module type DEQUE = sig
  type 'a t
 
  val empty : 'a t
  val is_empty : 'a t -> bool
 
  val to_list : 'a t -> 'a list
 
  val length : 'a t -> int
 
  val first : 'a t -> 'a
  val last  : 'a t -> 'a
 
  val cons : 'a -> 'a t -> 'a t
  val tail : 'a t -> 'a t
 
  val snoc : 'a -> 'a t -> 'a t
  val eject : 'a t -> 'a t
end
 
module Deque : DEQUE = struct
  type 'a t = 'a list * 'a list
 
  let rec take' acc n = function
    | [] -> acc
    | l::ls when n = 0 -> acc
    | l::ls -> take' (l::acc) (n-1) ls
 
  let take n l = List.rev @@ take' [] n l
 
  let rec drop n = function
    | [] -> []
    | l::ls when n = 0 -> l::ls
    | l::ls -> drop (n-1) ls
 
  let checkf (f, r) =
    let size_f, size_r = List.length f, List.length r in
    let c = 4 and
        diff = (size_f + size_r) / 2 in
     
    if size_f > c * size_r + 1 then begin
      let front = take diff f and
          rear = r @ List.rev (drop diff f) in
      (front, rear)
     
    end else if size_r > c * size_f + 1 then begin
      let front = f @ List.rev (drop diff r) and
          rear = take diff r in
      (front, rear)
     
    end else (f, r)
 
  let empty = ([], [])
   
  let is_empty = function
    | ([], []) -> true
    | _ -> false
 
  let to_list (f, r) = f @ List.rev r
 
  let length (f, r) = List.length f + List.length r
 
  let first = function
    | ([], [x]) -> x
    | (f, _) -> List.hd f
 
  let last = function
    | ([x], []) -> x
    | (_, r) -> List.hd r
 
  let cons x (f, r) = checkf (x::f, r)
  let snoc x (f, r) = checkf (f, x::r)
 
  let tail = function
    | ([], [x]) -> ([], [])
    | (_::fs, r) -> checkf (fs, r)
    | _ -> failwith "tail of empty deque"
 
  let eject = function
    | ([x], []) -> ([], [])
    | (f, _::rs) -> checkf (f, rs)
    | _ -> failwith "costam"
end

open Deque
type deque = (int * int) t
type kmax = int * deque * int

let kmaxempty k = (k, empty, 1)
let put kolejka e = 
  let rec aux = function
    | k, dq, age -> 
    if not (is_empty dq) && fst (last dq) <= e then 
      aux (k, eject dq, age)
    else 
      (k, snoc (e, age) dq, age + 1)
    in aux kolejka

let get kolejka = 
  let rec aux = function
    | k, dq, age -> 
    if snd (first dq) <= age - k then aux (k, tail dq, age )
    else fst (first dq)
  in aux kolejka