(* minimalizuj ||xi -xj| - d| dla danego d *)
open List

(* let minimalizuj l d = 
    let s = sort compare l in 
    let lepsze (a1, b1) (a2, b2) = 
        if abs ( d - abs (a1 - b1)) <= abs ( d - abs (a2 - b2)) then (a1, b1) else (a2, b2) in
    let rec aux (i, j) (bi, bj) =
        match (i, j) with 
        | ([], _) -> (bi, bj)
        | (_, []) -> (bi, bj)
        | (hi::ti, hj::tj) -> 
            if d > abs (hi - hj) then aux (i, tj) (lepsze (hi, hj) (bi, bj))
            else if d = abs (hi - hj) then  *)

let minimalizuj lista d = 
    let s = sort compare lista in 
    let rec pom l1 l2 acc =
        match (l1, l2) with
        | (_, []) -> acc
        | ([], _) -> acc
        | ((h1::t1), (h2:: t2)) -> 
            if h2 - h1 > d then pom t1 l2 (min acc (h2 - h1 - d))
            else pom l1 t2 (min acc (d - (h2 - h1))) in 
    let x = pom s s d 
    in abs (x - d);;

(* funkcja [x1; x2; ... xn] r -> c 
    moc zbioru {i: |xi - c| <= r} jest jak najwieksza *)

let odcinek lista r = 
    let d = 2 * r in
    let s = sort compare lista in 
    let maks (b1, c1) (b2, c2) =
        if b2 >= b1 then (b2, c2) else (b1, c1) in
    let rec pom l1 l2 k (best, c) = 
        match (l1, l2) with
        | (_, []) -> c
        | ([], _) -> c
        | (h1::t1, h2::t2) -> 
            if h2 - h1 <= 2*r then pom l1 t2 (k + 1) (maks (best, c) (k + 1, h1 + r))
            else pom t1 l2 (k - 1) (best, c)
        in 
    pom s s 0 (0, 0)

(* [x1; x2; ... xn] -> [k1; k2; ... kn] 
    od ilu lat na itej pozycji to jest najwieksza katastrofa *)
    (* kolejka monotoniczna *)