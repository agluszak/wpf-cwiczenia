
(* reprezentacja n w systemie (-2) *)
let convert n =
   let rec create v acc =
    match v with
    | 0 -> acc
    | x -> let rest = abs (x mod (-2)) in create ((x - rest) / (-2)) (rest::acc)
  in create n []

open List;;
let redukuj l =
  let lr = rev l in
  let rec aux rest acc max =
    match rest with
    | [] -> acc
    | h::t -> if h > max then aux t (max::acc) max
      else aux t (h::acc) h
  in (aux lr [] (hd lr));;
(* zabawka jasia! *)
let zabawka szerokosci walce =
  let zredukowane = redukuj szerokosci in
  let rec aux rsz rw acc =
    match (rsz, rw) with
    | ([], hw::tw) -> 0
    | (_, []) -> acc
    | (hsz::tsz, hw::tw) -> if hw > hsz then aux tsz rw (acc + 1) else aux tsz tw (acc + 1)
  in aux zredukowane walce 0;;
