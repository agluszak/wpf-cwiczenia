type 'a option = None | Some of 'a
type 'a tree = Null | Node of 'a * 'a tree * 'a tree * 'a option ref
let potomek drzewo = 
    let rec aux t h = 
    match t with
    | Null -> (None, -1)
    | Node(v, l, r, p) -> 
        let (lewy, lh) = aux l (h+1)
        and (prawy, rh) = aux r (h+1)
        in 
        if (lewy = None && prawy = None) then begin
            p:= None;
            (Some(v), h)
        end else if lh >= rh then begin
            p:= lewy;
            (lewy, lh)
        end else begin              
            p:= prawy;
            (prawy, rh)
        end;
    in aux drzewo 0; ()

(* kolowkium 2012/13 *)
type elem = {x: int; mutable prev: elem list};;
type lista = elem list;;

open List
let ustaw l = 
    let li = ref l in
    let odw = ref (rev l) in 
    while (!odw <> []) do 
        (hd !li).prev <- (!odw);
        odw := tl !odw;
        li := tl !li;
    done

open Array 
let rozne arr = 
    let srodek = ref 0 in
    let maleje = ref true in
    while (!srodek < ((length arr) - 1) && !maleje) do 
        if arr.(!srodek) < arr.(!srodek + 1) then maleje := false;
        srodek := !srodek + 1;
    done;
    !srodek

let fastryguj drzewo =
    let ost = ref Null in 
    let rec pom t = 
        match t with 
        | Null -> ()
        | Node(x, l, p, rx) -> 
        begin 
            pom l;
            (match !ost with
            | Null -> rx := t
            | Node(y, _, _, ry) -> 
                if x = y then begin
                    rx := !ry;
                    ry := t;
                end else begin
                    rx := t;
                end);
            ost := t;
            pom p;
        end
    in pom drzewo