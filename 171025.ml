let exists p l =
  List.fold_left (fun acc x -> acc || p x) false l

let heads l = List.flatten (List.map (fun x -> match x with |h::t -> [h] |[] -> []) l);;

let append l1 l2 =
  List.fold_left (fun acc h -> h::acc) l2 (List.rev l1);;

let append l1 l2 =
    List.fold_right (fun h acc -> h::acc) l1 l2;;

let suma l =
  function x -> List.fold_left (fun acc f ->  acc + (f x)) 0 l;;

let costam l = fold_left (fun a h -> if h < 0 then 0 else a + h) 0 l

let codrugi l = List.rev (List.fold_left (fun (drugi, acc) h -> if drugi then (false, h::acc) else (true, acc)) (false, []) l);;

let prefixy l = List.fold_left (fun (prefix, acc) h -> if h = 0 then (h::prefix, (h::prefix)::acc) else (h::prefix, acc)) ([], []) l;;
let pprefixy l = List.rev (List.map (fun e -> List.rev e) l)
