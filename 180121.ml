open Array
let latarnie l droga = 
    let arr = copy (of_list l) in 
    sort compare arr;
    let n = length arr in
    let przecina (x1, x2) (y1, y2) = x2 >= y1 in 
    let ilosc = ref 0 in 
    let najdalej start = 
        let i = ref (start + 1) in 
        let wynik = ref (-1) in 
        let odcinek = if start = -1 then (min_int, fst droga) else arr.(start) in begin
        while !i < n && przecina odcinek arr.(!i) do 
            wynik := !i;
            i := !i + 1;
        done;
        !wynik end in 
    let ostatni = ref (najdalej (-1)) in 
    let wynik = ref (-1) in 
    while !ostatni <> (-1) && !wynik = (-1) do 
        ilosc := !ilosc + 1;
        if przecina arr.(!ostatni) (snd droga, max_int) then wynik := !ilosc;
        ostatni := najdalej !ostatni; 
    done;
    !wynik;;

let test = [(1,4);(7,9);(-2,3);(4,9);(9,12);(0,5);(3,7)];;
latarnie test (-1,11)
    

 