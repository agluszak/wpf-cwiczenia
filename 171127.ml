type 'a deque = 'a list * int * 'a list * int

open List

let empty e = ([], 0, [], 0)
let push_front e q = match q with
    (fl, fs, bl, bs) -> (e::fl, 1 + fs, bl, bs)

let push_back e q = match q with
    (fl, fs, bl, bs) -> (fl, fs, e::bl, bs + 1)

let rec bal fl fs bl bs =
    if bs - fs > bs / 2 then 
        bal ((hd bl)::fl) (fs + 1) (tl bl) (bs - 1)
    else if fs - bs > fs / 2 then
        bal (tl fl) (fs - 1) ((hd fl)::bl) (bs + 1)
    else (fl, fs, bl, bs)

let balance q = match q with
    | ([], _, [], _) -> q
    (* | ([h], _, [], _) -> ([h], 1, [], 0)
    | ([], _, [h], _) -> ([], 0, [h], 1) *)
    | (fh::ft, fs, [], 0) -> bal (fh::ft) fs [] 0
    | ([], 0, bh::bt, bs) -> bal [] 0 (bh::bt) bs
    | _ -> q

let 