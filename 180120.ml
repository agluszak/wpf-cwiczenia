let biura input = 
    let open Array in 
    let n = length input in 
    let pracownicy = copy input in
    iter (fun arr -> sort compare arr) pracownicy;
    let odwiedzone = map (fun _ -> -1) input in 
    let skladowa = ref 0 in 
    let dfs v = 
        odwiedzone.(v) <- !skladowa;
        for i = 0 to n - 1 do begin
            let kontakt = ref 0 in 
            let j = ref 0 in
            while !j < n do 
                if !kontakt < length pracownicy.(i) then 
                    if pracownicy.(i).(!kontakt) = !j then 
                        kontakt := !kontakt + 1;
                    