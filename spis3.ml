let latarnie l droga = 
    let arr = copy (of_list l) in 
    sort compare arr;
    let n = length arr in
    let przecina (x1, x2) (y1, y2) = x2 >= y1 in 
    let ilosc = ref 0 in 
    let najdalej start = 
        let i = ref (start + 1) in 
        let wynik = ref (-1) in 
        let odcinek = if start = -1 then (min_int, fst droga) else arr.(start) in begin
        while !i < n && przecina odcinek arr.(!i) do 
            wynik := !i;
            i := !i + 1;
        done;
        !wynik end in 
    let ostatni = ref (najdalej (-1)) in 
    let wynik = ref (-1) in 
    while !ostatni <> (-1) && !wynik = (-1) do 
        ilosc := !ilosc + 1;
        if przecina arr.(!ostatni) (snd droga, max_int) then wynik := !ilosc;
        ostatni := najdalej !ostatni; 
    done;
    !wynik;;

let srodek tree = 
    let rec aux t = 
        match t with
        | Null -> Null
        | Node(_, l, r) -> 
            let lt = aux l in 
            let rt = aux r in
            let l0 = match lt with
            | Null -> 0 
            | Node((l1, p1), _, _) -> 1 + max l1 p1 in 
            let p0 = match rt with 
            | Null -> 0
            | Node((l2, p2), _, _) -> 1 + max l2 p2 in
            Node((l0, p0), lt, rt) 

let pinionzki l v = 
    let arr = Array.make (v+1) max_int in 
    arr.(0) <- 0;
    List.iter (fun moneta ->
        let i = ref moneta in 
        while !i <= v do 
            arr.(!i) <- min (1 + arr.(!i - moneta)) (arr.(!i));
            i := !i + 1;
        done
    ) l;
    arr

(* graf nieskierowany, znaleźć długość najdłuższej ścieżki o rosnących wierzchołkach *)
let najdluzsza_sciezka lista = 
    (* długość najdłuższej ścieżki kńczącej się w i  *)
    let sciezki = Array.make (List.length lista) 0 in 
    List.iteri (fun i neighbours -> List.iter (fun j -> if j > i then 
        sciezki.(j) <- (max (sciezki.(j)) (sciezki.(i) + 1))) neighbours) lista;
    Array.fold_left (fun acc el -> max acc el) 0 sciezki

type teren = Rownina | Dolina | Gora | Stok
let gory_i_doliny input = 
    let open Array in
    let result = map (fun arr -> map (fun _ -> Rownina) arr) input in 
    let visited = map (fun arr -> map (fun _ -> false) arr) input in 
    let xsize = length input in 
    let ysize = length input.(0) in 
    let get tab (x, y) = tab.(x).(y) in
    let set tab (x, y) v = tab.(x).(y) <- v in 
    let neighbours pos =
        let is_ok (x, y) = x >= 0 && y >= 0 && x < xsize && y < ysize in 
        let possible_neighbours = [(0, 1); (1, 0); (-1, 0); (0, -1)] in 
        let pair_sum (x1, y1) (x2, y2) = (x1 + x2, y1 + y2) in  
        let open List in 
        map (fun i -> pair_sum pos i) possible_neighbours |> 
            filter is_ok in
     let handle pos = 
        if not (get visited pos) then 
        let acc = ref [pos] in 
        let gora = ref true in 
        let dolina = ref true in
        let rec dfs pos = 
            let neighbours = neighbours pos in 
                begin
                set visited pos true;
                List.iter (fun neighbour -> 
                    let my_height = get input pos in 
                    let neighbour_height = get input neighbour in
                    if neighbour_height < my_height then begin
                        dolina := false; 
                    end else if neighbour_height > my_height then begin 
                        gora := false; 
                    end else begin 
                        acc := pos::!acc;
                        if not (get visited neighbour) then dfs neighbour
                    end
                ) neighbours; 
                end
            in begin dfs pos;
            let terrain = match (!gora, !dolina) with
            | true, true -> Rownina
            | false, true -> Dolina
            | true, false -> Gora
            | false, false -> Stok
            in List.iter (fun pos -> set result pos terrain) !acc 
        end in 
    for i = 0 to xsize - 1 do 
        for j = 0 to ysize - 1 do 
            handle (i, j);
        done;
    done;
    result

(* trzymamy tablicę minimalnych kosztów sklejenia kawałka o długoći o i do j *)
(* dodajemy to co jest w lewej do tego co w prawej i max dlugosci *)
let antyczny_kijek sticks = 
    let open List in
    let n = length sticks in 
    let pref = Array.make n 0 in 
    let rec fill i = function 
        | [] -> ()
        | h::t -> pref.(i) <- pref.(i-1) + h; fill (i+1) t in 
        pref.(0) <- hd sticks;
        fill 1 (tl sticks);
        let dp = Array.make_matrix (n+1) n max_int in 
        for i = 0 to (n-1) do 
            dp.(0).(i) <- 0;
        done;
        for i = 1 to (n-1) do
            for j = 0 to (n-1) do 
                for k = j to (j+i-1) do 
                    dp.(i).(j) <- min 
                        (dp.(i).(j)) 
                        (dp.(k-j+1).(j) 
                            + dp.(j+i-k-1).(k+1) 
                            + (max 
                                (pref.(j+i-1) - pref.(k)) 
                                (pref.(k) - pref.(j-1))
                            )
                        );
                done;
            done;
        done;
    dp.(n).(0)

let najdluzszy_rosnacy_podciag ciag = 
    let open Array in
    let n = length ciag in 
    let arr = make n 1 in 
    let naj = ref 0 in 
    for i = 0 to (n-1) do 
        for j = 0 to (i-1) do  
                if ciag.(j) < ciag.(i) then begin
                    arr.(i) <- max (arr.(i) + 1) arr.(i);
                    naj := max arr.(i) !naj;
                end;
            done;
        done;
    !naj

(* para (maks z tym wierzchołkiem, maks w ogóle) *)
(* dzieci (ai, bi)  max_z = ten + suma ai, max = suma bi*)
type tree = Node of (int * tree list)
let rec impreza (Node(fajnosc, podwladni)) = 
    let lp = List.map impreza podwladni in 
    List.fold_left (fun (z, bez) (z_tamtym, bez_tamtego) -> 
    (z + bez_tamtego, bez + max z_tamtym bez_tamtego))
        (fajnosc, 0) lp;;

