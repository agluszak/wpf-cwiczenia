(* [x1; x2; ... xn] -> [k1; k2; ... kn] 
    od ilu lat na itej pozycji to jest najwieksza katastrofa *)
    (* kolejka monotoniczna *)
open List;;

let rec pom k wcz a s = 
    match wcz with
    | [] -> (s::k, [(a,s)])
    | (w, p)::tail -> if a < w then (s::k, (a, s)::wcz) else pom k tail a (s + p) 

let katastrofy l = 
    let rec aux l (k, wcz) =
        match l with 
        | [] -> rev k
        | (a::tail) -> aux tail (pom k wcz a 1)
    in aux l ([], []);;

let katastrofy l = 
    let rec pom wynik kolejka nowa od_ilu_lat =
        match kolejka with
        | [] -> (od_ilu_lat::wynik, [(nowa, od_ilu_lat)])
        | (stara, od_ilu_lat_stara)::tail -> 
            if nowa < stara then (od_ilu_lat::wynik, (nowa, od_ilu_lat)::kolejka)
            else pom wynik tail nowa (od_ilu_lat + od_ilu_lat_stara)
    in let rec aux rest (wynik, kolejka) =
        match rest with
        | [] -> rev wynik
        | (katastrofa::tail) -> aux tail (pom wynik kolejka katastrofa 1)
    in aux l ([], [])

let katastrofy l = 
    
(* [x1; x2; ...; xn] posortowana. k razy! bierzemy dwa najmniejsze, dodajemy i wkładamy do listy
    trzymamy sobie listę i heapa. bierzemy najmniejszy z listy i najmniejszy z heapa (lub odpowiednio dwa) i dodajemy. 
    złożoność będzie k*logk *)


let kubeczki l k = 
    let wez_min lista kolejka = 
    if is_empty kolejka then match lista with
        | h::t -> (h, t, kolejka)
        | [] -> failwith "nie ma z czego"
    else let min_kolejki = get_min kolejka in 
        match lista with
        | h::t -> if h < min_kolejki then (h, t, kolejka) else (min_kolejki, lista, del_min kolejka)
        | [] -> (min_kolejki, [], del_min kolejka)
    in let rec aux lista kolejka rest =
        if rest = 0 then let (min, _, _) = wez_min lista kolejka in min 
        else let (pierwsze, n_l, n_k) = wez_min lista kolejka in 
            let (drugie, f_l, f_k) = wez_min n_l n_k in 
            aux f_l (push f_k (pierwsze + drugie)) (rest - 1) in 
        aux l Empty k;;