type 'a tree = Node of 'a * 'a tree list
open List

let rosnaca tree = 
    let rosnie v lista = match lista with
    | [] -> true
    | h::_ -> v > h in
    let rec aux t (naj, naj_len) (poprzednia, poprzednia_len) =
        match t with 
        | Node(v, lista) ->
        let (obecna, obecna_len) = if rosnie v poprzednia then (v::poprzednia, poprzednia_len + 1) else ([v], 1) in 
        match lista with
        | [] -> if obecna_len >= naj_len then (obecna, obecna_len) else (naj, naj_len)
        | _ -> fold_left (fun (max, max_len) drzewko -> 
            let (nowa, nowa_len) = aux drzewko (max, max_len) (obecna, obecna_len) in 
            if nowa_len >= max_len then (nowa, nowa_len) else (max, max_len)) (naj, naj_len) lista in 
        aux tree ([], 0) ([], 0) |> fst |> rev

type option = None | Some of int
type state = Plus | Minus | Zero
let zwieksz zero = match zero with 
    | None -> None
    | Some(x) -> Some(x + 1)
let okresl x = if x > 0 then Plus else if x < 0 then Minus else Zero
(* let prawie lista =
    let aux (zero, stan, dlg, maks) head =
    match (okresl head, stan) with
    | (Plus, Minus) -> (zwieksz zero, Plus, dlg + 1, max maks (dlg + 1))
    | (Minus, Plus) -> (zwieksz zero, Minus, dlg + 1, max maks (dlg + 1))
    | (Zero, _) -> match zero with
        | None -> (Some(0), Zero, dlg + 1, max maks (dlg +1))
        | Some(x) -> (Some(0), Zero, 1, maks)
    | (Plus, Plus) -> (None, Plus, 1, maks)
    | (Minus, Minus) -> (None, Minus, 1, maks)
    | (Plus, Zero) -> (Some(1), Plus, 1, maks)
    | (Minus, Zero) -> (Some(1), Minus, 1, maks) *)

let sort l = l;;
let trojkat lista = 
    let rec aux l byla=
    match l with
    | h1::h2::h3::t -> aux (h2::h3::t) (byla || h1 + h2 > h3)
    | _ -> byla in 
    aux (sort lista) false;;