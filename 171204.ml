open Array

let rotacja a k = 
    let move l r kl = 
        for i = 0 to (kl-1) do 
            let temp = a.(l+i) in 
            begin 
                a.(l+i) <- a.(r+i-kl);
                a.(r+i-1) <- temp;
            end
        done in 
    let rec aux l r kl = 
        let n = r - l in 
        if n <> kl then
            if kl < n/2 then begin
                move l r kl;
                aux l (r-kl) kl
            end else begin 
                move l r (n-kl);
                aux (r-kl) r (2*kl-n)
            end
    in aux 0 (length a) k;;

(* mamy sobie szachownicę NxN i ustawioną pewną ilość wież (dostajemy listę par jako współrzędne).
    mamy pokolorować wieże tak że jak się mogą zbić to są na ten sam kolor *)
module type FIND_UNION = sig
  type 'a set
  val make_set : 'a ->'a set
  val find : 'a set ->'a
  val equivalent : 'a set ->'a set ->bool
  val union : 'a set ->'a set ->unit
  val elements : 'a set ->'a list
  val n_of_sets : unit->int
end;;

module Find_Union : FIND_UNION = struct
  (* Typ klas elementów typu 'a. *)
  type 'a set = {
    elem : 'a; (* Element klasy. *)
    up : 'a set ref; (* Przodek w drzewie find-union. *)
    mutable rank : int; (* Ranga w drzewie find-union. *)
    mutable next : 'a set list (* Lista potomków w pewnym drzewie rozpinającym
                                  klasę. *)
  }
  (* Licznik klas. *)
  let sets_counter = ref 0
  (* Liczba wszystkich klas. *)
  let n_of_sets () = !sets_counter
  (* Tworzy nową klasę złożoną tylko z danego elementu. *)
  let make_set x =
    let rec v = { elem = x; up = ref v; rank = 0; next = [] }
    in begin
      sets_counter := !sets_counter + 1;
      v
    end
  (* Znajduje korzeń drzewa,kompresując ścieżkę. *)
  let rec go_up s =
    if s == !(s.up) then s
    else begin
      s.up := go_up !(s.up);
      !(s.up)
    end
  (* Znajduje reprezentanta danej klasy. *)
  let find s =
    (go_up s).elem
  (* Sprawdza,czy dwa elementy są równoważne. *)
  let equivalent s1 s2 =
    go_up s1 == go_up s2
  (* Scala dwie dane (rozłączne) klasy. *)
  let union x y =
    let fx = go_up x
    and fy = go_up y
    in
    if not (fx == fy) then begin
      if fy.rank >fx.rank then begin
        fx.up := fy;
        fy.next <- fx :: fy.next
      end else begin
        fy.up := fx;
        fx.next <- fy :: fx.next;
        if fx.rank = fy.rank then fx.rank <- fy.rank + 1
      end;
      sets_counter := !sets_counter - 1
    end
  (* Lista elementów klasy. *)
  let elements s =
    let acc = ref []
    in
    let rec traverse s1 =
      begin
        acc := s1.elem :: !acc;
        List.iter traverse s1.next
      end
    in begin
      traverse (go_up s);
      !acc
    end
end;;

(* let wieze n lista = 
    let cmpx (x1, y1) (x2, y2) = compare x1 x2 in
    let cmpy (x1, y1) (x2, y2) = compare y1 y2 in 
    let posortowana = copy lista in 
    begin
        stable_sort cmpx posortowana;
        stable_sort cmpy posortowana
    end
    in  *)