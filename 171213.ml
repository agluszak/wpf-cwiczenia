type 'a tree = Null | Node of 'a * 'a tree * 'a tree * 'a tree ref 

(* fastryga: 'a -> unit. dostajemy drzewo bst. łączymy wierzchołki o tych samych wartościach w listy cykliczne *)
let fastryga drzewo =
    let ost = ref Null in 
    let rec pom t = 
        match t with 
        | Null -> ()
        | Node(x, l, p, rx) -> 
        begin 
            pom l;
            (match !ost with
            | Null -> rx := t
            | Node(y, _, _, ry) -> 
                if x = y then begin
                    rx := !ry;
                    ry := t;
                end else begin
                    rx := t;
                end);
            ost := t;
            pom p;
        end
    in pom drzewo

(* ciąg (x1, x2, ..., xn) xj jest 0 albo 1 *)
(* dostajemy na wejściu trójki (i, j, s) mówi że suma xi + x(i+1) + ... + xj = s mod 2 *)
(* funkcja ciąg: int -> (int * int * int) list -> int (jakie jest największa liczba k , że da się utworzyć poprawny ciąg z k trójek ) *)
(* 1 <= i <= j <= s *)

(* yi = (x1 + ... + xi) mod 2 
y0 = 0
i sprawdzamy dla dla danej trójki czy yj - y(i-1) = s mod 2 *)

let ciag n l = 
    let z = Array.init (n+1) (fun i -> make_set i) in (* zbiory *)
    let p = Array.make (n+1) (-1) in (* przeciwne *)
    let k = ref 0 in (* ile trojek przetworzylismy *)
    let l = ref l in 
    let koniec = ref false in 
    while !l <> [] && not !koniec do 
        let (i, j, s) = List.hd !l in 
        let (i, j) = (find z.(i-1), find z.(j)) in 
        if i = j then (if s = 1 then koniec := true) 
        else if (p.(i) = j) then (if s = 0 then kon := true )
        else if s = 0 then 
            (union z.(i) z.(j);
            if p.(i) <> -1 && p.(j) <> -1 then 
                union z.(p.(i)) z.(p.(j)));
            if p.(i) || p.(j)
            (* wyciągnąć reprezentatna *)
