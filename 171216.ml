(* przychodzi nowa katastrofa. porównujemy się z headem kolejki. jak jesteśmy mniejsi, to dopisujemy się na koniec i wstawiamy 1 do wyniku. 
jak jesteśmy więksi, to monotonizujemy kolejkę i obliczamy od ilu lat jesteśmy najwięksi. *)

open List

let katastrofy_po_ludzku l = 
    let monotonizuj kolejka katastrofa = 
        let rec aux rest wiek = 
            match rest with
            | [] -> [(katastrofa, wiek)]
            | (stara, stara_wiek)::t -> 
                if stara > katastrofa then 
                    (katastrofa, wiek)::rest
                else 
                    aux t (wiek + stara_wiek)
        in aux kolejka 1
    in 
    let rec aux rest wyniki kolejka = 
        match rest with
        | [] -> rev wyniki 
        | katastrofa::t -> 
            let nowa_kolejka = monotonizuj kolejka katastrofa in 
            aux t ((nowa_kolejka |> hd |> snd)::wyniki) nowa_kolejka in 
    aux l [] [];;

let x = katastrofy_po_ludzku;;

module type DEQUE = sig
  type 'a t
 
  val empty : 'a t
  val is_empty : 'a t -> bool
 
  val to_list : 'a t -> 'a list
 
  val length : 'a t -> int
 
  val first : 'a t -> 'a
  val last  : 'a t -> 'a
 
  val cons : 'a -> 'a t -> 'a t
  val tail : 'a t -> 'a t
 
  val snoc : 'a -> 'a t -> 'a t
  val eject : 'a t -> 'a t
end
 
module Deque : DEQUE = struct
  type 'a t = 'a list * 'a list
 
  let rec take' acc n = function
    | [] -> acc
    | l::ls when n = 0 -> acc
    | l::ls -> take' (l::acc) (n-1) ls
 
  let take n l = List.rev @@ take' [] n l
 
  let rec drop n = function
    | [] -> []
    | l::ls when n = 0 -> l::ls
    | l::ls -> drop (n-1) ls
 
  let checkf (f, r) =
    let size_f, size_r = List.length f, List.length r in
    let c = 4 and
        diff = (size_f + size_r) / 2 in
     
    if size_f > c * size_r + 1 then begin
      let front = take diff f and
          rear = r @ List.rev (drop diff f) in
      (front, rear)
     
    end else if size_r > c * size_f + 1 then begin
      let front = f @ List.rev (drop diff r) and
          rear = take diff r in
      (front, rear)
     
    end else (f, r)
 
  let empty = ([], [])
   
  let is_empty = function
    | ([], []) -> true
    | _ -> false
 
  let to_list (f, r) = f @ List.rev r
 
  let length (f, r) = List.length f + List.length r
 
  let first = function
    | ([], [x]) -> x
    | (f, _) -> List.hd f
 
  let last = function
    | ([x], []) -> x
    | (_, r) -> List.hd r
 
  let cons x (f, r) = checkf (x::f, r)
  let snoc x (f, r) = checkf (f, x::r)
 
  let tail = function
    | ([], [x]) -> ([], [])
    | (_::fs, r) -> checkf (fs, r)
    | _ -> failwith "tail of empty deque"
 
  let eject = function
    | ([x], []) -> ([], [])
    | (f, _::rs) -> checkf (f, rs)
    | _ -> failwith "costam"
end

(* musisz się porównać z ostatnim?? *)
(* open Printf
let kmax_zad_9 k l =
    let monotonizuj dq nowy wiek = 
        if Deque.is_empty dq then 
            Deque.cons (nowy, wiek) Deque.empty 
        else 
            let rec aux rest = 
            let (stary, _) = Deque.first dq in 
            if stary <= nowy then 
                Deque.cons (nowy, wiek)monotonizuj (Deque.tail dq) nowy wiek 
            else
                dq
    in
    let wyrzuc_stary dq wiek wynik = 
        if Deque.is_empty dq then
            (dq, wynik)
        else 
            let (stary, stary_wiek) = Deque.last dq in 
            if wiek - stary_wiek > k then
                (Deque.eject dq, stary::wynik)
            else
                (dq, wynik)
    in
    let rec aux rest dq wiek wynik = 
        printf "wiek: %d;" wiek; List.iter (fun (a, b) -> printf "(%d %d)" a b ) (Deque.to_list dq); printf "\n";
        match rest with
        | [] ->  rev (if not (Deque.is_empty dq) then ((Deque.last dq |> fst)::wynik) else wynik)
        | h::t -> 
        let (nowa_kolejka, nowy_wynik) = wyrzuc_stary dq wiek wynik in 
        let zmonotonizowana = monotonizuj nowa_kolejka h wiek in 
        aux t zmonotonizowana (wiek + 1) nowy_wynik
    in aux l Deque.empty 1 [];; *)

    
