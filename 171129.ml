module type COUNTER = sig 
    type counter 
    val make: unit -> counter
    val inc: counter -> int
    val reset: unit -> unit
end

module Counter : COUNTER = struct
    type counter = (int ref) * (int ref)
    let global_reset = ref 0
    let make = fun () -> (ref 0, ref 0)
    let inc = function (value, last_reset) ->
        if !last_reset < !global_reset 
            then begin value := 1;
                last_reset := !global_reset;
                1
                end
        else begin 
            value := !value + 1;
            !value
            end
    let reset = fun () -> global_reset := !global_reset + 1

end

open Array

let cykl arr = 
    let n = length arr in
    let odwiedzone = make n false in 
    let rec przejdz start i dlugosc = 
        odwiedzone.(i) <- true;
        if i = start then dlugosc
        else przejdz start (arr.(i)) (dlugosc + 1) 
    in let rec loop i maks = 
        if i = n  then maks
        else if not (odwiedzone.(i)) then loop (i + 1) (max (przejdz i (arr.(i)) 1) maks) 
        else loop (i + 1) maks
    in loop 0 1;;

