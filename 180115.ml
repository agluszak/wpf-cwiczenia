let pinionzki l v = 
    let arr = Array.make (v+1) max_int in 
    arr.(0) <- 0;
    List.iter (fun moneta ->
        let i = ref moneta in 
        while !i <= v do 
            arr.(!i) <- min (1 + arr.(!i - moneta)) (arr.(!i));
            i := !i + 1;
        done
    ) l;
    arr

(*  *)
(* trzymamy tablicę minimalnych kosztów sklejenia kawałka o długoći o i do j *)
(* dodajemy to co jest w lewej do tego co w prawej i max dlugosci *)
let antyczny_kijek sticks = 
    let open List in
    let n = length sticks in 
    let pref = Array.make n 0 in 
    let rec fill i = function 
        | [] -> ()
        | h::t -> pref.(i) <- pref.(i-1) + h; fill (i+1) t in 
        pref.(0) <- hd sticks;
        fill 1 (tl sticks);
        let dp = Array.make_matrix (n+1) n max_int in 
        for i = 0 to (n-1) do 
            dp.(0).(i) <- 0;
        done;
        for i = 1 to (n-1) do
            for j = 0 to (n-1) do 
                for k = j to (j+i-1) do 
                    dp.(i).(j) <- min (dp.(i).(j)) (dp.(k-j+1).(j) + dp.(j+i-k-1).(k+1) + (max (pref.(j+i-1) - pref.(k)) ( pref.(k) - pref.(j-1))));
                done;
            done;
        done;
    dp.(n).(0)

let najdluzszy_rosnacy_podciag ciag = 
    let open Array in
    let n = length ciag in 
    let arr = make n 1 in 
    let naj = ref 0 in 
    for i = 0 to (n-1) do 
        for j = 0 to (i-1) do  
                if ciag.(j) < ciag.(i) then begin
                    arr.(i) <- max (arr.(i) + 1) arr.(i);
                    naj := max arr.(i) !naj;
                end;
            done;
        done;
    !naj

(* para (maks z tym wierzchołkiem, maks w ogóle) *)
(* dzieci (ai, bi)  max_z = ten + suma ai, max = suma bi*)
type tree = Node of (int * tree list)
let rec impreza (Node(fajnosc, podwladni)) = 
    let lp = List.map impreza podwladni in 
    List.fold_left (fun (z, bez) (z_tamtym, bez_tamtego) -> 
    (z + bez_tamtego, bez + max z_tamtym bez_tamtego))
        (fajnosc, 0) lp;;

