(* graf nieskierowany, znaleźć długość najdłuższej ścieżki o rosnących wierzchołkach *)
let najdluzsza_sciezka lista = 
    (* długość najdłuższej ścieżki kńczącej się w i  *)
    let sciezki = Array.make (List.length lista) 0 in 
    List.iteri (fun i neighbours -> List.iter (fun j -> if j > i then sciezki.(j) <- (max (sciezki.(j)) (sciezki.(i) + 1))) neighbours) lista;
    Array.fold_left (fun acc el -> max acc el) 0 sciezki

type teren = Rownina | Dolina | Gora | Stok
let gory_i_doliny input = 
    let open Array in
    let result = map (fun arr -> map (fun _ -> Rownina) arr) input in 
    let visited = map (fun arr -> map (fun _ -> false) arr) input in 
    let xsize = length input in 
    let ysize = length input.(0) in 
    let get tab (x, y) = tab.(x).(y) in
    let set tab (x, y) v = tab.(x).(y) <- v in 
    let neighbours pos =
        let is_ok (x, y) = x >= 0 && y >= 0 && x < xsize && y < ysize in 
        let possible_neighbours = [(0, 1); (1, 0); (-1, 0); (0, -1)] in 
        let pair_sum (x1, y1) (x2, y2) = (x1 + x2, y1 + y2) in  
        let open List in 
        map (fun i -> pair_sum pos i) possible_neighbours |> 
            filter is_ok in
     let handle pos = 
        if not (get visited pos) then 
        let acc = ref [pos] in 
        let gora = ref true in 
        let dolina = ref true in
        let rec dfs pos = 
            let neighbours = neighbours pos in 
                begin
                set visited pos true;
                List.iter (fun neighbour -> 
                    let my_height = get input pos in 
                    let neighbour_height = get input neighbour in
                    if neighbour_height < my_height then begin
                        dolina := false; 
                    end else if neighbour_height > my_height then begin 
                        gora := false; 
                    end else begin 
                        acc := pos::!acc;
                        if not (get visited neighbour) then dfs neighbour
                    end
                ) neighbours; 
                end
            in begin dfs pos;
            let terrain = match (!gora, !dolina) with
            | true, true -> Rownina
            | false, true -> Dolina
            | true, false -> Gora
            | false, false -> Stok
            in List.iter (fun pos -> set result pos terrain) !acc 
        end in 
    for i = 0 to xsize - 1 do 
        for j = 0 to ysize - 1 do 
            handle (i, j);
        done;
    done;
    result



    