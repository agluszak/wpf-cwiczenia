open List;;

let fold_left_via_right f a l =
    fold_right (fun h p -> function x -> p (f x h)) l (fun x -> x) a;;

let map_via_fold_right f l = fold_right (fun h t -> (f h)::t) l [];;

let filter_via_fold_right p l = fold_right (fun head acc -> if p head then head::acc else acc) l [];;

let sito l =
    match l with
        | [] -> []
        | h::t -> filter (fun x -> x mod h <> 0) t;;

let gen n = 
    let rec pom acc k = if k < 2 then acc else pom (k::acc) (k - 1)
    in pom [] n;;

let eratostenes n = 
    let rec erat l = if l = [] then [] else (hd l)::(erat (sito l))
    in erat (gen n);;

type 'a tree = Node of 'a tree * 'a * 'a tree | Empty;;

let rec fold_tree f acc t = 
    match t with
    | Empty -> acc
    | Node(l, v, r) -> f (fold_tree f acc l) v (fold_tree f acc r);;

let map_tree f t = 
    fold_tree (fun l v r -> Node(l, f v, r)) Empty t;;

type 'a nbtree = NbNode of 'a * 'a nbtree list;;

let rec fold_nbtree f (NbNode (x, l)) = 
    f x (map (fold_nbtree f) l);;

let map_nbtree f t = fold_nbtree (fun x l ->NbNode (f x,l)) t;;

let nbwidoczne t = 
    let rec merge (element: int) (l: (int -> int) list) (maks: int) = 
        if element < maks then
            fold_left (fun acc h -> acc + h maks) 0 l
        else
            fold_left (fun acc h -> acc + h element) 0 l + 1
    in
        fold_nbtree merge t (-1);;

(* wartość w węźle drzewa jest widocza jeżeli na ścieżce od tego węzła 
    do korzenia nie ma większej wartości *)
let widoczne (t: int tree) = 
    (* zwraca funkcję, która jako argument przyjmuje maksimum na ścieżce
        od korzenia do korzenia danego poddrzewa
        i oblicza liczbę elementów widocznych w poddrzewie *)
    let rec merge (lewy: int -> int) (element: int) (prawy: int -> int) =
        fun (maks: int) ->
        if element < maks then 
            lewy maks + prawy maks
        else 
            lewy element + prawy element + 1
    in match t with
    | Empty -> 0
    | Node(_, v, _) -> (fold_tree merge (fun _ -> 0) t) (v);;

(* wzrost [1; 2; 1; 2; 3];; *)
(* # wzrost [3;4;0;(-1);2;3;7;6;7;8];; *)
let wzrost (l: int list) =
    let aux (najdluzszy, naj_len, obecny, obecny_len) (head: int) =
        match obecny with
        | [] -> (najdluzszy, naj_len, [head], 1)
        | h::_ -> if head > h then (najdluzszy, naj_len, head::obecny, obecny_len + 1) 
            else if (obecny_len) >= (naj_len) then (obecny, obecny_len, [head], 1)
            else (najdluzszy, naj_len, [head], 1)
    in let (najdluzszy, naj_len, obecny, obecny_len) = (fold_left aux ([], 0, [], 0) l)
    in rev (if obecny_len >= naj_len then obecny else najdluzszy) ;;

let prextrema (l: int list) = 
    let aux  (minimum, maximum, acc) head =
        if head < minimum then (head, maximum, head::acc)
        else if head > maximum then (minimum, head, head::acc)
        else (minimum, maximum, acc)
    in let (_, _, lista) = fold_left aux (hd l , hd l, [hd l]) l
    in rev lista;;

    let ciekawe l =
        let aux (obecny, acc) head = 
            match obecny with
            | [] -> ([head], acc)
            | h::t -> if head <> h then (head::obecny, acc)
                else ([head], obecny::acc)
        in let (obecny, lista) = fold_left aux ([], []) l
        in rev (map rev (obecny::lista));;

let rec quicksort l =
    match l with
    | [] -> []
    | h::[] -> [h]
    | h::t -> let niewieksze = filter (fun x -> x < h) t
        and wieksze = filter (fun x -> x >= h) t
    in (quicksort niewieksze) @ (h::quicksort wieksze);;

let roznicowy l = 
    match l with
    | [] -> []
    | [a] -> [a]
    | h::t ->
    let aux (poprzedni, ciag) head =
        (head, (head - poprzedni)::ciag)
    in tl (rev (snd (fold_left aux (h, []) l)));;

let dekompresja l n = 
    let rec aux lista reszta =
        match lista with
        | [] -> failwith "niema"
        | (l, s, r)::t ->
            if reszta >= r then aux t (reszta - r)
            else let pos = reszta mod s in 
            nth l pos 
        in aux l n;;

let kompresuj l =
    let aux (liczba, ilosc, ciag) head =
        if liczba <> head then
            let zakodowana = ilosc * (2 * liczba - 1) in
            (head, 1, zakodowana::ciag)
        else
            (liczba, 2*ilosc, ciag)
    in let (liczba, ilosc, ciag) = fold_left aux (hd l, 1, []) (tl l) in 
    rev ((ilosc * ( 2 * liczba - 1))::ciag);;

type 'a option = Some of 'a | None;;

let perm l n =
    let rec aux lista =
        match lista with
        | [] -> failwith "pusta permutacja"
        | h::[] -> if h = n then Some(hd l) else None
        | h::p::t -> if h = n then Some(p) else aux (p::t) in 
    aux l;;

let buduj_permutacje l =
    function x ->
    let rec aux lista =
    match lista with
    | [] -> x
    | h::t -> match (perm h x) with
        | None -> aux t
        | Some(n) -> n
    in aux l;;

type monoton = Ros | Mal | Dunno

let mono m poprzedni head = 
    match (m, compare head poprzedni) with
    | (Ros, -1) -> (1, Dunno)
    | (Ros, _) -> (0, Ros)
    | (Dunno, 0) -> (0, Dunno)
    | (Dunno, 1) -> (0, Ros)
    | (Dunno, -1) -> (0, Mal)
    | (Mal, 1) -> (1, Dunno)
    | (Mal, _) -> (0, Mal)

let monotons l = 
    let rec aux lista monot poprzedni n =
        match lista with
        | [] -> n
        | h::t -> let (inc, nmonot) = mono monot poprzedni h in
         aux t nmonot h (n + inc) in
    aux (tl l) Dunno (hd l) 1;;

