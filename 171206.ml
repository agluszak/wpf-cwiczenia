open Array

(* mamy sobie szachownicę NxN i ustawioną pewną ilość wież (dostajemy listę par jako współrzędne).
    mamy pokolorować wieże tak że jak się mogą zbić to są na ten sam kolor *)

module type FIND_UNION = sig
  type 'a set
  val make_set : 'a ->'a set
  val find : 'a set ->'a
  val equivalent : 'a set ->'a set ->bool
  val union : 'a set ->'a set ->unit
  val elements : 'a set ->'a list
  val n_of_sets : unit->int
end;;

module Find_Union : FIND_UNION = struct
  (* Typ klas elementów typu 'a. *)
  type 'a set = {
    elem : 'a; (* Element klasy. *)
    up : 'a set ref; (* Przodek w drzewie find-union. *)
    mutable rank : int; (* Ranga w drzewie find-union. *)
    mutable next : 'a set list (* Lista potomków w pewnym drzewie rozpinającym
                                  klasę. *)
  }
  (* Licznik klas. *)
  let sets_counter = ref 0
  (* Liczba wszystkich klas. *)
  let n_of_sets () = !sets_counter
  (* Tworzy nową klasę złożoną tylko z danego elementu. *)
  let make_set x =
    let rec v = { elem = x; up = ref v; rank = 0; next = [] }
    in begin
      sets_counter := !sets_counter + 1;
      v
    end
  (* Znajduje korzeń drzewa,kompresując ścieżkę. *)
  let rec go_up s =
    if s == !(s.up) then s
    else begin
      s.up := go_up !(s.up);
      !(s.up)
    end
  (* Znajduje reprezentanta danej klasy. *)
  let find s =
    (go_up s).elem
  (* Sprawdza,czy dwa elementy są równoważne. *)
  let equivalent s1 s2 =
    go_up s1 == go_up s2
  (* Scala dwie dane (rozłączne) klasy. *)
  let union x y =
    let fx = go_up x
    and fy = go_up y
    in
    if not (fx == fy) then begin
      if fy.rank >fx.rank then begin
        fx.up := fy;
        fy.next <- fx :: fy.next
      end else begin
        fy.up := fx;
        fx.next <- fy :: fx.next;
        if fx.rank = fy.rank then fx.rank <- fy.rank + 1
      end;
      sets_counter := !sets_counter - 1
    end
  (* Lista elementów klasy. *)
  let elements s =
    let acc = ref []
    in
    let rec traverse s1 =
      begin
        acc := s1.elem :: !acc;
        List.iter traverse s1.next
      end
    in begin
      traverse (go_up s);
      !acc
    end
end;;

open Find_Union;;
open List;;
type cos = {el: int * int; s: (int * int) set}
let wieze lista = 
    let kolory = List.map (fun x -> {el = x; s = make_set x}) lista in
    let cmpx {el = (x1, y1); s =_} {el = (x2, y2); s = _} = compare x1 x2 in
    let cmpy {el = (x1, y1); s =_} {el = (x2, y2); s = _} = compare y1 y2 in
    let posortowana_po_x = stable_sort cmpx kolory in
    let rec aux rest f = 
      match rest with
      | a::b::t -> if f a.el = f b.el then union a.s b.s; aux (b::t) f
      | _ -> ()
    in  
      aux posortowana_po_x fst;
      let posortowana_po_y = stable_sort cmpy posortowana_po_x in
      aux posortowana_po_y snd;
      n_of_sets();;

(* Na drzewie wisi n małpek ponumerowanych od 1 do n. Małpka z nr 1 trzyma się gałęzi
ogonkiem. Pozostałe małpki albo są trzymane przez inne małpki, albo trzymają się
innych małpek, albo jedno i drugie równocześnie. Każda małpka ma dwie przednie
łapki, każdą może trzymać co najwyżej jedną inną małpkę (za ogon). Rozpoczynając
od chwili 0, co sekundę jedna z małpek puszcza jedną łapkę. W ten sposób niektóre
małpki spadają na ziemię, gdzie dalej mogą puszczać łapki (czas spadania małpek jest
pomijalnie mały).
Zaprojektuj algorytm, który na podstawie opisu tego która małpka trzyma którą, oraz
na podstawie opisu łapek puszczanych w kolejnych chwilach, dla każdej małpki wyznaczy
moment, kiedy spadnie ona na ziemię.
 *)

 (* 1. obliczamy zbiór tych które nigdy się nie puściły
 2. ustalamy początkowe klasy abstrakcji
 3. symulacja chwytania (puszczanie od tyłu) *)

 (* [(1,2), (1,2), (3,1), (3,4)] *)
 (* pierwsza trzyma drugą, pierwsza trzyma drugą drugą ręką, trzecia trzyma pierwszą, trzycia trzyma czwartą *)
 (* w tej samej postaci lista kolejnych puszczeń *)

(* A - B *)
let diff a b = 
  let a_sorted = List.sort compare a 
  and b_sorted = List.sort compare b in
  let rec aux a_rest b_rest acc =
    match (a_rest, b_rest) with
    | ([], []) -> acc
    | (ha::ta, []) -> aux ta [] (ha::acc)
    | ([], hb::tb) -> acc
    | (ha::ta, hb::tb) -> if ha < hb then aux ta (hb::tb) (ha::acc) 
                          else if ha = hb then aux ta tb acc 
                          else aux ta tb (ha::acc)
  in aux a_sorted b_sorted []

let polacz laczenia klasy = 
  List.iter (fun (a, b) -> union klasy.(a) klasy.(b)) laczenia

(* (a,b) oznacza, ze a trzyma b lub a puszcza b. Zawsze taka kolejnosc *)
let malpki n trzymania puszczenia = 
  let klasy = Array.init n (fun x -> make_set x) in 
  let nierozlaczne = diff trzymania puszczenia in 
  polacz nierozlaczne klasy;
  let czas = length puszczenia in 
  let momenty = Array.init n (fun x -> -1) in 
  puszczenia |> rev |> iteri 
    begin fun i (a, b) -> 
      elements (klasy.(a)) |> iter begin fun x ->
        momenty.(x) <- (czas - i)
      end;
      union klasy.(a) klasy.(b);
    end;
  let pierwsza = klasy.(0) in
  klasy |> Array.iteri (fun i kl ->
    if not (equivalent kl pierwsza) then begin 
      momenty.(i) <- 0
    end);
  momenty
  


