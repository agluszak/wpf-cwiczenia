type 'a option = None | Some of 'a
type 'a tree = Null | Node of 'a * 'a tree * 'a tree * 'a option ref
open Printf;;

let fastryguj drzewo = 
    let nast = ref None in
    let rec aux t = 
    match t with
    | Null -> ()
    | Node(v, l, r, p) -> 
        begin
            aux r;
            p := !nast;
            nast := Some(v);
            aux l;
        end
    in aux drzewo;;


let rec przemnoz lista = 
    match lista with
    | [] -> [[]]
    | l::ll -> let ll = przemnoz ll in 
        List.fold_left (fun wynik x ->
            List.fold_left (fun wynik iloczyn -> 
                (x::iloczyn)::wynik) wynik ll) [] l;;

let lewe_liscie drzewo =
    let rec aux t = 
    match t with 
    | Null -> None
    | Node(v, l, r, p) -> 
            match (aux l, aux r) with
            | None, None -> p := None; Some(v)
            | None, Some(x) -> p:= Some(x); Some(x)
            | Some(y), _ -> p:=Some(y); Some(y)
    in aux drzewo; ()
            
let t = Node (1, Node(3, Null, Null, ref None), Node (2, Null, Null, ref None), ref None)

let w_lewo drzewo = 
    let q = Queue.create() in
    Queue.add (drzewo, 1) q;
    while not (Queue.is_empty q) do 
        match Queue.take q with
        | (Null, _) -> ()
        | (Node(v, l, r, p), h) -> 
        if not (Queue.is_empty q) then
            match Queue.peek q with
            | (Node(vv, _, _, _), hh) when hh = h -> p := Some(vv)
            | _ -> p:=None
        else
            p := None;
        if r <> Null then Queue.add (r, h + 1) q;
        if l <> Null then Queue.add (l, h + 1) q;
    done
            

