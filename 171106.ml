type 'a tree = Node of 'a tree * 'a * 'a tree | Empty;;

let rec fold_tree f acc t = 
    match t with
    | Empty -> acc
    | Node(l, v, r) -> f (fold_tree f acc l) v (fold_tree f acc r);;

let wysokosc t = fold_tree (fun l _ r -> 1 + max l r) 0 t;;

let rec shit_tree f t =
    match t with
    | Empty -> Empty
    | Node(l,v,r) as n -> Node(shit_tree f l, f n, shit_tree f r)

let map_tree f t = 
    fold_tree (fun l v r -> Node(l, f v, r)) Empty t;;

let wysokosc_eff tr =
    let rec aux t h =
    match t with
    | Empty -> Node(Empty, 0, Empty)
    | Node(l, v, r) -> Node(aux l (h+1), h, aux r (h+1))
    in aux tr 1;;

let czy_avl t =
    let wysokosci = shit_tree wysokosc t in wysokosci

let infix t =
    let rec aux tr =
    match tr with
    | Empty -> []
    | Node(l, v, r) -> (aux l) @ [v] @ (aux r) in 
    aux t;; 

let infix2 t = 
    let rec aux tr l = 
    match tr with
    | Empty -> l
    | Node(l, v, r) -> 