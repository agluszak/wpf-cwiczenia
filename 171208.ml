let silnia n = 
    let w = ref 1 in
    let i = ref 2 in
    while !i <= n do 
        w := !w * !i;
        i := !i + 1;
    done;
    !w